# Mobucks Sdk

Mobucks™ enables app owners to embed text, banner, audio, video targeted advertisements to their mobile applications. Initially, inventory placements will be defined and setup together with the Inventory Owner (= slicing & dicing). Each placeholder is uniquely identified by the property ‘plid’ and has configuration regarding the ad serving type (XML, Vast feed etc.), pricing and content rendering. Each premium campaign that is setup via Mobucks can use one or more placeholders to be displayed

# Installation
 Add the mobuckssdk.framework in the Embedded Binaries (Xcode -> General)
 
* Objective c only: set "Always Embed Swift Standard Libraries" to yes (Xcode -> Build Settings)

Add in the info.plist file the following lines
```xml
<key>NSAppTransportSecurity</key>
<dict>
<key>NSAllowsArbitraryLoads</key><true/>
</dict>
```
Check file  [here](https://bitbucket.org/application_repo/ios-sdk-demo/src/master/Demo%20App/Info.plist)
# Banners
### Import the sdk 
```swift
import mobucks_ios_sdk
```
### Create the banner view
Can be created programmatically like this
```swift
let bannerView =  MBBannerAdView(frame: rect, placementId:"<placementId>",uid:"<uid>",password:"<password>",adSize:size)
```
### Add event listeners
```swift
      /*
        Called when ad is successfully loaded
      */
      bannerView.onAdLoaded { (uiView:UIView) in
        print("Ad loaded")
      }
      /*
        Called when ad is clicked
      */
      bannerView.onAdViewClicked { (uiView:UIView) in
        print("Ad Clicked")
      }
      /*
        Called when ad failed to load
      */
      bannerView.onAdFailed{ (error:Error) in
        print(error)
      }
```
### Optional configuration
```swift
/*
    setting preferred  ad size (optional)
*/
bannerView.setAdSize(CGSize(width:320,height:50))
/*
    setting ad max size (optional)
*/
bannerView.setMaxSize(CGSize(width:320,height:50))
/*
    setting interval for auto refresh (optional)
    time is in millis.
*/
bannerView.setAutoRefreshInterval(15000)
/*
    If true  the banner will reload on resume (optional)
 */
bannerView.setShouldReloadOnResume(true)
/*
    setting the targeting age (optional)
*/
bannerView.setTargetingAge(20)
/*
    setting the targeting gender (optional)
*/
bannerView.setTargetingGender(.male)
/*
    setting the targeting language (optional)
 */
bannerView.setTargetingLanguage("el")
```
### Request banner ad
```swift
 /*
    request banner ad
 */
bannerView.loadAd()
```

Complete example [here](https://bitbucket.org/application_repo/ios-sdk-demo/src/master/Demo%20App/BannerViewController.swift)

# Interstitial
### Import the sdk 
```swift
import mobucks_ios_sdk
```
### Create the interstitial view
Can be created programmatically like this
```swift

let interstitialView =  MBInterstitialAdView( placementId:"<placementId>",uid:"<uid>",password:"<password>",adSize:size)
```
### Add event listeners
```swift
      /*
       Called when ad is successfully loaded
       */
      interstitialView.onAdLoaded { (uiView:UIView) in
          print("Ad loaded")
      }
      /*
       Called when ad is clicked
      */
      interstitialView.onAdViewClicked { (uiView:UIView) in
          print("Ad Clicked")
      }
      /*
       Called when ad failed to load
      */
      interstitialView.onAdFailed{ (error:Error) in
          print(error)
      }
```
### Optional configuration
```swift
/*
    setting the targeting age (optional)
*/
interstitialView.setTargetingAge(20)
/*
    setting the targeting gender (optional)
*/
interstitialView.setTargetingGender(.male)
/*
    setting the targeting language (optional)
 */
interstitialView.setTargetingLanguage("el")
```
### Request interstitial ad
```swift
 /*
    request interstitial ad
 */
interstitialView.loadAd()
```

### Show the ad
You can check if ad is ready with
```swift
interstitialView.isReady()
```
and use
```swift
interstitialView.show()
```
to show the ad


Complete example [here](https://bitbucket.org/application_repo/ios-sdk-demo/src/master/Demo%20App/InterstitialViewController.swift)

# Video
### Import the sdk 
```swift
import mobucks_ios_sdk
```
### Create the video view
Can be created programmatically like this
```swift
let videoView =  MBVideoAdView(frame: videoRect, placementId:"<placementId>",uid:"<uid>",password:"<password>",adSize:adSize)
```
### Add event listeners
```swift
      /*
       Called when ad is successfully loaded
       */
      videoView.onAdLoaded { (uiView:UIView) in
          print("Ad loaded")
      }
      /*
       Called when ad is clicked
      */
      videoView.onAdViewClicked { (uiView:UIView) in
          print("Ad Clicked")
      }
      /*
       Called when ad failed to load
      */
      videoView.onAdFailed{ (error:Error) in
          print(error)
      }
```
### Optional configuration
```swift
 /*
   setting the targeting age (optional)
 */
videoView.setTargetingAge(19)
/*
    setting the targeting gender (optional)
*/
videoView.setTargetingGender(.male)
/*
    setting the targeting language (optional)
*/
videoView.setTargetingLanguage("el")
```
### Request video ad
```swift
/*
    request video ad
 */
videoView.loadAd()
```
Complete example [here](https://bitbucket.org/application_repo/ios-sdk-demo/src/master/Demo%20App/VideoViewController.swift)
