//
//  VideoViewController.swift
//  Demo App
//
//  Created by OTM User.
//  Copyright © 2018 otm. All rights reserved.
//

import Foundation
import UIKit
import mobucks_ios_sdk
class VideoViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get the screen size so we can center our 300x50 example ad.
        let  screenRect  = UIScreen.main.bounds
        let  centerX     = (screenRect.size.width / 2) - 150
        let  centerY     = (screenRect.size.height / 2) - 150
        
        let  videoRect  = CGRect(x:centerX, y:centerY, width:300, height:300)
        let  adSize  = CGSize(width:300, height:300)
        
        //Create the video view
       let videoView =  MBVideoAdView(frame: videoRect, placementId:"<placementId>",uid:"<uid>",password:"<password>",adSize:adSize)
        
        /*
         Called when ad is successfully loaded
         */
        videoView.onAdLoaded { (uiView:UIView) in
            print("Ad loaded")
        }
        /*
         Called when ad is clicked
         */
        videoView.onAdViewClicked { (uiView:UIView) in
            print("Ad Clicked")
        }
        /*
         Called when ad failed to load
         */
        videoView.onAdFailed{ (error:Error) in
            print(error)
        }
        
        /*
         setting the targeting age (optional)
         */
        videoView.setTargetingAge(19)
        /*
         setting the targeting gender (optional)
         */
        videoView.setTargetingGender(.male)
        /*
         setting the targeting language (optional)
         */
        videoView.setTargetingLanguage("el")
        
        videoView.loadAd()
        
        self.view.addSubview(videoView)
        
    }
    
    
}

