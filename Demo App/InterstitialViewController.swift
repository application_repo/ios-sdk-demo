//
//  InterstitialViewController.swift
//  Demo App
//
//  Created by OTM User.
//  Copyright © 2018 otm. All rights reserved.
//

import Foundation
import UIKit
import mobucks_ios_sdk
class InterstitialViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get the screen size so we can center our 300x50 example ad.
        
        // Set up some sizing variables we'll need when we create our ad view.
        let  size  = CGSize(width:1080, height:1920)
        
        //Create the interstitial view
        let interstitialView =  MBInterstitialAdView( placementId:"<placementId>",uid:"<uid>",password:"<password>")
        
        /*
         Called when ad is successfully loaded
         */
        interstitialView.onAdLoaded { (uiView:UIView) in
            //Show the ad
            interstitialView.show()
        }
        /*
         Called when ad is clicked
         */
        interstitialView.onAdViewClicked { (uiView:UIView) in
            print("Ad Clicked")
        }
        /*
         Called when ad failed to load
         */
        interstitialView.onAdFailed{ (error:Error) in
            print(error)
        }
        
        /*
         setting the targeting age (optional)
         */
        interstitialView.setTargetingAge(20)
        /*
         setting the targeting gender (optional)
         */
        interstitialView.setTargetingGender(.male)
        /*
         setting the targeting language (optional)
         */
        interstitialView.setTargetingLanguage("el")
        
        /*
         request interstitial ad
         */
        interstitialView.loadAd()
        
        
        
    }
    
    
}

