//
//  BannerViewController.swift
//  Demo App
//
//  Created by OTM User.
//  Copyright © 2018 otm. All rights reserved.
//

import Foundation
import UIKit
import mobucks_ios_sdk
class BannerViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get the screen size so we can center our 300x50 example ad.
        let  screenRect  = UIScreen.main.bounds
        let  centerX     = (screenRect.size.width / 2) - 150
        let  centerY     = (screenRect.size.height / 2) - 25
        
        // Set up some sizing variables we'll need when we create our ad view.
        let  rect  = CGRect(x:centerX, y:centerY, width:300, height:50)
        let  size  = CGSize(width:300, height:50)
        
        //Create the banner view
        let bannerView =  MBBannerAdView(frame: rect, placementId:"<placementId>",uid:"<uid>",password:"<password>",adSize:size)
        
        /*
         Called when ad is successfully loaded
         */
        bannerView.onAdLoaded { (uiView:UIView) in
            print("Ad loaded")
        }
        /*
         Called when ad is clicked
         */
        bannerView.onAdViewClicked { (uiView:UIView) in
            print("Ad Clicked")
        }
        /*
         Called when ad failed to load
         */
        bannerView.onAdFailed{ (error:Error) in
            print(error)
        }
        
        /*
         setting preferred  ad size (optional)
         */
        bannerView.setAdSize(CGSize(width:320,height:50))
        /*
         setting ad max size (optional)
         */
        bannerView.setMaxSize(CGSize(width:320,height:50))
        /*
         setting interval for auto refresh (optional)
         time is in millis.
         */
        bannerView.setAutoRefreshInterval(15000)
        /*
         If true  the banner will reload on resume (optional)
         */
        bannerView.setShouldReloadOnResume(true)
        /*
         setting the targeting age (optional)
         */
        bannerView.setTargetingAge(20)
        /*
         setting the targeting gender (optional)
         */
        bannerView.setTargetingGender(.male)
        /*
         setting the targeting language (optional)
         */
        bannerView.setTargetingLanguage("el")
        
        /*
         request banner ad
        */
        bannerView.loadAd()
        
        self.view.addSubview(bannerView)
        
        
        
    }
    
    
}

